//1. Дані, збережені в localStorage, залишаються в браузері навіть після закриття вкладки чи браузера. Ці дані залишаються в localStorage до тих пір, поки не будуть явно видалені або не буде очищений кеш браузера.
//Дані, збережені в sessionStorage, існують тільки під час дії сесії браузера. Якщо ви закриєте вкладку або браузер, дані будуть видалені.
//2.Замість прямого зберігання потрібно використовувати  токени для аутентифікації. Токени можуть бути збережені в безпечніших місцях. Перевірка джерела запитань. Використання HTTPS. пртрібно Обмежити доступ до localStorage та sessionStorage.
//3. Коли сеанс завершується, браузер автоматично видаляє всі дані, збережені в sessionStorage, безпосередньо пов'язані з поточним вікном чи вкладкою браузера. 

function setDarkMode() {
    const body = document.querySelector('.main-page');
    const bookCards = document.querySelectorAll('.product-container');
    const mainTitle = document.querySelector('.main-page__title')
    const btnsAdd = document.querySelectorAll('.main-page__product-container__btn-add-to-basket')

    const wasDarkMode = localStorage.getItem('darkmode') === "true";

    body.classList.toggle('main-page-dark', wasDarkMode);

    bookCards.forEach(bookCard => {
        bookCard.classList.toggle('product-container__dark', wasDarkMode);
    });

    btnsAdd.forEach(btnsAdd => {
        btnsAdd.classList.toggle('main-page__product-container__btn-add-to-basket-darkMode', wasDarkMode);
    });

    mainTitle.classList.toggle('dark-mode title', wasDarkMode)
}

function darkmode() {
    const wasDarkMode = localStorage.getItem('darkmode') === "true";

    localStorage.setItem('darkmode', !wasDarkMode);
    setDarkMode();
}

const btn = document.querySelector('.header__btn-b');
btn.addEventListener('click', darkmode);
document.addEventListener('DOMContentLoaded', setDarkMode);